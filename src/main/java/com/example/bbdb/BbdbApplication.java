package com.example.bbdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BbdbApplication {

	public static void main(String[] args) {
		SpringApplication.run(BbdbApplication.class, args);
	}
}
